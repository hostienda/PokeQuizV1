(function() {

    angular.module('PokeModule')

    .controller('PokeDetailsController', ['$q','$scope','$stateParams','pokeService','$cordovaSocialSharing','$filter','$timeout', function($q, $scope, $stateParams, pokeService, $cordovaSocialSharing,$filter,$timeout){

        var mayuscula = $filter('mayus');
        var primerMayus = $filter('mayuscula');

        $scope.pokemon={};
        $scope.id = $stateParams.pokeId;
        var id = $stateParams.pokeId;
            if(id>=1 && id<=9){
                id='00'+id;
            }
            if(id>=10 && id<=99){
                id='0'+id;
            }

        pokeService.getPokemon(id).then(function(data){
            $scope.pokemon = data;
            $scope.maxVal = 300; 

            var nombre = mayuscula(data.name);
            //var imag = "http://assets.pokemon.com/assets/cms2/img/pokedex/full/"+data.id+".png";
            var name = primerMayus($stateParams.Name);

            $scope.shareAnywhere2= function () {
                var mensaje= name+" usó la nueva App PokeQuiz y descubrió que es un "+nombre+'.';
                $cordovaSocialSharing.share(mensaje,"App PokeQuiz",null," Descubre tu también que Pokemons eres, https://play.google.com/store/apps/dev?id=6214168289555124073&hl=es");
            };

            function shareAnywhere() {
                var mensaje= name+" usó la nueva App PokeQuiz y descubrió que es un "+nombre+'.';
                $cordovaSocialSharing.share(mensaje,"App PokeQuiz",null," Descubre tu también que Pokemons eres, https://play.google.com/store/apps/dev?id=6214168289555124073&hl=es");
            };

            var mensaje1 = $timeout(shareAnywhere, 4000);
            $scope.$on('$destroy', function () { 
                $timeout.cancel(mensaje1); 
            });

            $scope.getPercentage = function (valor) {
                return (parseInt(valor) / parseInt($scope.maxVal) * 100);
            };

            $scope.percentageStyle1= {
                width : $scope.getPercentage(data.stats.hp) + '%'
            };
            $scope.percentageStyle2= {
                width : $scope.getPercentage(data.stats.attack) + '%'
            };
             $scope.percentageStyle3= {
                width : $scope.getPercentage(data.stats.defense) + '%'
            };
             $scope.percentageStyle4= {
                width : $scope.getPercentage(data.stats.sp_atk) + '%'
            };
             $scope.percentageStyle5= {
                width : $scope.getPercentage(data.stats.sp_def) + '%'
            };
             $scope.percentageStyle6= {
                width : $scope.getPercentage(data.stats.speed) + '%'
            };
            $scope.addClass='progress-bar-bar';
            
        });
    }]);
    
})();
