(function() {

    angular.module('PokeModule', ['PokeapiPokeModule','Filters','ngCordova'])

    .config(function($stateProvider) {
        $stateProvider
            .state('app.pokemons', {
            url: '/pokemons/',
            views:{
                'content':{
                    templateUrl: 'js/app/pokemons/Pokemons/pokemons.html',
                    controller: 'PokeController'
                }
            }
        })

            .state('app.poke-detail', {
            url: '/pokemons/detail/:pokeId/:Name',
            views:{
                'content':{
                    templateUrl: 'js/app/pokemons/PokeDetails/PokeDetails.html',
                    controller: 'PokeDetailsController'
                }
            }
        })
    });

})();