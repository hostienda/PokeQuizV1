  (function() {

  	angular.module('PokeModule')
  	.controller('PokeController', ['$scope', 'pokeService','$state','$timeout','$cordovaInAppBrowser', '$translate',function($scope, pokeService,$state,$timeout,$cordovaInAppBrowser,$translate){

      $scope.dias = ["1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30","31"]

      $scope.quiz = { nombre:'',dia:'',mes:''};
      $scope.data = { color:'',valor:''};
      $scope.lista = [];
      var currentLang = ($translate.proposedLanguage() || $translate.use());

      if(currentLang==='es'){
        $scope.colores1 = [ {name:'Azul', valor:5},
                            {name:'Rosa', valor:10},
                            {name:'Blanco', valor:15}
                          ];

        $scope.colores2 = [ {name:'Verde', valor:20},
                            {name:'Morado', valor:25},
                            {name:'Rojo', valor:30}
                          ];
      }else{
        $scope.colores1 = [ {name:'Blue', valor:5},
                            {name:'Pink', valor:10},
                            {name:'White', valor:15}
                          ];

        $scope.colores2 = [ {name:'Green', valor:20},
                            {name:'Purple', valor:25},
                            {name:'Red', valor:30}
                          ];
      }

      $scope.exists = function (color, list){
        return list.indexOf(color) > -1;
      };

  		$scope.enviar = function(form){
        if($scope.quiz.dia==undefined || $scope.quiz.mes==undefined || $scope.quiz.nombre==undefined || $scope.quiz.nombre=="" || $scope.data.color.valor==undefined || $scope.message2){
          
              if(currentLang==='es'){
                $scope.message="Todos los Campos son requeridos!";
              }else{
                $scope.message="All fields are required!";
              }

        }else{
          var total = 0;

          total = (parseInt($scope.quiz.dia) * parseInt($scope.quiz.mes));
          total = total * $scope.quiz.nombre.length;
          total = total * $scope.data.color.valor;
          total = parseInt(total);

          while(total>=721){
            total=(total/2);
          }
          var name=$scope.quiz.nombre;
          var id = total.toFixed();
          $state.go("app.poke-detail",{pokeId:id,Name:name});

          var limp = $timeout(limpiar, 2000);
          $scope.$on('$destroy', function () { $timeout.cancel(limp); });

         if (form) {
            form.$setPristine();
            form.$setUntouched();
             $scope.message="";
          }
        }
      };

      function limpiar() {
        $scope.quiz = { nombre:'',dia:'',mes:''};
        $scope.data = { color:'',valor:''};
      };

      $scope.verificar = function (valor){
        if(valor!='' && valor!=undefined){
          var dias= $scope.quiz.dia;
          meses=[{mes:1, max:31},
                {mes:2, max:29},
                {mes:3, max:31},
                {mes:4, max:30},
                {mes:5, max:31},
                {mes:6, max:30},
                {mes:7, max:31},
                {mes:8, max:31},
                {mes:9, max:30},
                {mes:10, max:31},
                {mes:11, max:30},
                {mes:12, max:31}];
            
            if(dias > meses[valor-1].max){
              if(currentLang==='es'){
                $scope.message2="El dia no corresponde con el mes!";
              }else{
                $scope.message2="The day does not correspond with the month!";
              }
            }else{
              $scope.message2="";
            }
        }
      };

      $scope.quiz= function(){
        $state.go("app.pokemons");
      };

      $scope.abrir = function(){
        window.open('https://play.google.com/store/apps/dev?id=6214168289555124073&hl=es','_system','location=yes'); 
      };

  }])
})();